"use strict";
const images = document.querySelectorAll('.image-to-show');
images.forEach(image => {
    images[0].style.display = '';
    image.style.display = 'none';

});
let stop = document.getElementById('stop');
let start = document.getElementById('start');

function showImage(array, duration, index) {
    let timer = setInterval(replacePicture, duration);

    function replacePicture() {
        images.forEach(image => {

            image.style.display = 'none';
        });
        let item = array[index];
        item.style.display = '';
        index++;
        if (index >= array.length) {
            index = 0;
        }
    }


    stop.addEventListener('click', () => {
        clearInterval(timer);
        start.disabled = false;
        stop.disabled = true;

    })

    start.addEventListener('click', () => {
        timer = setInterval(replacePicture, 3000);
        start.disabled = true;
        stop.disabled = false;
    })
}

showImage(images, 3000, 1);